<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'api_platform.messenger.data_persister' shared service.

include_once $this->targetDirs[3].'/vendor/api-platform/core/src/DataPersister/DataPersisterInterface.php';
include_once $this->targetDirs[3].'/vendor/api-platform/core/src/DataPersister/ContextAwareDataPersisterInterface.php';
include_once $this->targetDirs[3].'/vendor/api-platform/core/src/Bridge/Symfony/Messenger/DispatchTrait.php';
include_once $this->targetDirs[3].'/vendor/api-platform/core/src/Bridge/Symfony/Messenger/DataPersister.php';

return $this->privates['api_platform.messenger.data_persister'] = new \ApiPlatform\Core\Bridge\Symfony\Messenger\DataPersister(($this->privates['api_platform.metadata.resource.metadata_factory.cached'] ?? $this->getApiPlatform_Metadata_Resource_MetadataFactory_CachedService()), ($this->services['message_bus'] ?? $this->load('getMessageBusService.php')));
