<?php

declare(strict_types=1);

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    public const ROOT_PACKAGE_NAME = '__root__';
    public const VERSIONS          = array (
  'antishov/doctrine-extensions-bundle' => 'v1.4.0@1420b1bc4100b51b8ebbcb5f11b59f3ba3fa5775',
  'api-platform/api-pack' => 'v1.2.2@ca7ca22f30bcfcdb2493ec9e8b7b5dba1271608a',
  'api-platform/core' => 'v2.4.7@53793261113b18931ce8a9e2f174f59fdaa0c1c5',
  'behat/transliterator' => 'v1.3.0@3c4ec1d77c3d05caa1f0bf8fb3aae4845005c7fc',
  'doctrine/annotations' => '1.10.3@5db60a4969eba0e0c197a19c077780aadbc43c5d',
  'doctrine/cache' => '1.10.1@35a4a70cd94e09e2259dfae7488afc6b474ecbd3',
  'doctrine/collections' => '1.6.5@fc0206348e17e530d09463fef07ba8968406cd6d',
  'doctrine/common' => '2.13.3@f3812c026e557892c34ef37f6ab808a6b567da7f',
  'doctrine/dbal' => 'v2.9.3@7345cd59edfa2036eb0fa4264b77ae2576842035',
  'doctrine/doctrine-bundle' => '1.11.2@28101e20776d8fa20a00b54947fbae2db0d09103',
  'doctrine/doctrine-cache-bundle' => '1.4.0@6bee2f9b339847e8a984427353670bad4e7bdccb',
  'doctrine/doctrine-migrations-bundle' => '2.1.2@856437e8de96a70233e1f0cc2352fc8dd15a899d',
  'doctrine/event-manager' => '1.1.0@629572819973f13486371cb611386eb17851e85c',
  'doctrine/inflector' => '1.3.1@ec3a55242203ffa6a4b27c58176da97ff0a7aec1',
  'doctrine/instantiator' => '1.3.1@f350df0268e904597e3bd9c4685c53e0e333feea',
  'doctrine/lexer' => '1.0.2@1febd6c3ef84253d7c815bed85fc622ad207a9f8',
  'doctrine/migrations' => '2.2.1@a3987131febeb0e9acb3c47ab0df0af004588934',
  'doctrine/orm' => 'v2.7.3@d95e03ba660d50d785a9925f41927fef0ee553cf',
  'doctrine/persistence' => '1.3.7@0af483f91bada1c9ded6c2cfd26ab7d5ab2094e0',
  'doctrine/reflection' => '1.2.1@55e71912dfcd824b2fdd16f2d9afe15684cfce79',
  'fig/link-util' => '1.1.1@c038ee75ca13663ddc2d1f185fe6f7533c00832a',
  'gedmo/doctrine-extensions' => 'v2.4.41@e55a6727052f91834a968937c93b6fb193be8fb6',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'nelmio/cors-bundle' => '1.5.6@10a24c10f242440211ed31075e74f81661c690d9',
  'nikic/php-parser' => 'v4.5.0@53c2753d756f5adb586dca79c2ec0e2654dd9463',
  'ocramius/package-versions' => '1.4.2@44af6f3a2e2e04f2af46bcb302ad9600cba41c7d',
  'ocramius/proxy-manager' => '2.1.1@e18ac876b2e4819c76349de8f78ccc8ef1554cd7',
  'phpdocumentor/reflection-common' => '2.1.0@6568f4687e5b41b054365f9ae03fcb1ed5f2069b',
  'phpdocumentor/reflection-docblock' => '4.3.4@da3fd972d6bafd628114f7e7e036f45944b62e9c',
  'phpdocumentor/type-resolver' => '1.0.1@2e32a6d48972b2c1976ed5d8967145b6cec4a4a9',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/link' => '1.0.0@eea8e8662d5cd3ae4517c9b864493f59fca95562',
  'psr/log' => '1.1.3@0f73288fd15629204f9d42b7055f72dacbe811fc',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'symfony/asset' => 'v4.2.12@82f5349982967842aeb7d2f8e876ac900be24f14',
  'symfony/cache' => 'v4.2.12@28286385757fa08c249254541ad814a8ccaff0f7',
  'symfony/config' => 'v4.2.12@637588756df1fb1c801833aead54a7153967c0cb',
  'symfony/console' => 'v4.2.12@fc2e274aade6567a750551942094b2145ade9b6c',
  'symfony/contracts' => 'v1.1.8@f51bca9de06b7a25b19a4155da7bebad099a5def',
  'symfony/debug' => 'v4.2.12@f832ecacfb00688b498eedf9b10784e401d969a6',
  'symfony/dependency-injection' => 'v4.2.12@39fe71bc481483f255e388a658c8f1104fec037e',
  'symfony/doctrine-bridge' => 'v4.2.12@5e8d62453eda38a523a12cc918031cc8f48e4b65',
  'symfony/dotenv' => 'v4.2.12@6163f061011009655da1bc615b38941bc460ef1b',
  'symfony/event-dispatcher' => 'v4.2.12@852548c7c704f14d2f6700c8d872a05bd2028732',
  'symfony/expression-language' => 'v4.2.12@ea23981c1dee4f2f901fce8345d34614237d57ca',
  'symfony/filesystem' => 'v4.2.12@1bd7aed2932cedd1c2c6cc925831dd8d57ea14bf',
  'symfony/finder' => 'v4.2.12@cecff7164790b0cd72be2ed20e9591d7140715e0',
  'symfony/flex' => 'v1.8.4@7df5a72c7664baab629ec33de7890e9e3996b51b',
  'symfony/framework-bundle' => 'v4.2.12@ac12bd9f104bb2dc319b09426e767ecfa95688da',
  'symfony/http-foundation' => 'v4.2.12@2ae778ff4a1f8baba7724db1ca977ada3b796749',
  'symfony/http-kernel' => 'v4.2.12@8a7c5ef599466af6e972c705507f815df9c490ae',
  'symfony/inflector' => 'v4.2.12@275e54941a4f17a471c68d2a00e2513fc1fd4a78',
  'symfony/maker-bundle' => 'v1.19.0@bea8c3c959e48a2c952cc7c4f4f32964be8b8874',
  'symfony/messenger' => 'v4.2.12@b7eb1cb7f5ad4451565971afd92912d376548d01',
  'symfony/orm-pack' => 'v1.0.8@c9bcc08102061f406dc908192c0f33524a675666',
  'symfony/polyfill-mbstring' => 'v1.17.1@7110338d81ce1cbc3e273136e4574663627037a7',
  'symfony/property-access' => 'v4.2.12@c3532a4bdb785446970148da68e03dc11514e256',
  'symfony/property-info' => 'v4.2.12@c5d4e006eb3fb386c5b68cd3b1dbb3f0cc6516df',
  'symfony/routing' => 'v4.2.12@1174ae15f862a0f2d481c29ba172a70b208c9561',
  'symfony/security-bundle' => 'v4.2.12@d731987aabf6b28b86faeb158973b0d0c22a0a09',
  'symfony/security-core' => 'v4.2.12@3ec42b5dbeee143715da686539751ea762dd8564',
  'symfony/security-csrf' => 'v4.2.12@ff004ea4d215fd4a740f6d6ca9643ff92326c16c',
  'symfony/security-guard' => 'v4.2.12@ef6d700e6be1ca75bc2788068c62506ab11461bd',
  'symfony/security-http' => 'v4.2.12@8024422eeaca7b0b33ce2900b2f75e20259de7aa',
  'symfony/serializer' => 'v4.2.12@8a2974c10e8eb8eb2d36a28c1bd68eb0b411cc60',
  'symfony/stopwatch' => 'v4.2.12@b1a5f646d56a3290230dbc8edf2a0d62cda23f67',
  'symfony/twig-bridge' => 'v4.2.12@708a993aaf3b979738d6e0a12bf157f02fc94998',
  'symfony/twig-bundle' => 'v4.2.12@db06490aeabba37dfc55a53fbf901c75e0d4f7b0',
  'symfony/validator' => 'v4.2.12@7b4485db55b7ea1a0d13d126c2781313017f815f',
  'symfony/var-exporter' => 'v4.2.12@f5be0592bb191debd278cf7e16413df0c978de8f',
  'symfony/web-link' => 'v4.2.12@47b8188b4bb8d24eef0bb287b0737d5b84a6cab8',
  'symfony/yaml' => 'v4.2.12@9468fef6f1c740b96935e9578560a9e9189ca154',
  'twig/twig' => 'v2.12.5@18772e0190734944277ee97a02a9a6c6555fcd94',
  'webmozart/assert' => '1.9.0@9dc4f203e36f2b486149058bade43c851dd97451',
  'willdurand/negotiation' => 'v2.3.1@03436ededa67c6e83b9b12defac15384cb399dc9',
  'zendframework/zend-code' => '3.4.1@268040548f92c2bfcba164421c1add2ba43abaaa',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'paragonie/random_compat' => '2.*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  'symfony/polyfill-ctype' => '*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  'symfony/polyfill-iconv' => '*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  'symfony/polyfill-php71' => '*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  'symfony/polyfill-php70' => '*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  'symfony/polyfill-php56' => '*@45c9480dbfe19010abff30820cf88cc4538eb1ea',
  '__root__' => 'dev-master@45c9480dbfe19010abff30820cf88cc4538eb1ea',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException If a version cannot be located.
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
