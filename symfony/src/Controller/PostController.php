<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Email\NewMentionsEmail;
class PostController extends AbstractController
{


    public function __invoke(NewMentionsEmail $mentionEmail ,Comment $comment) : Comment
    {
        $user = $comment->getCreator()->find($mentionEmail->getUserId());

        return $user;

    }

}
