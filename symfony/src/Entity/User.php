<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A user
 *
 * @ApiResource
 * @ORM\Entity
 */
class User
{

    /**
     * @var int The id of this user.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string Username of the user
     *
     * @ORM\Column(type="text")
     * @Groups({"post"})
     */
    public $username;

    /**
     * @var string Email of the user
     *
     * @ORM\Column(type="text")
     */
    public $email;


    public function getId(): ?int
    {
        return $this->id;
    }

}




